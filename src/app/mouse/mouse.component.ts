import { Component } from '@angular/core';
import { Coordinate } from '../services/coordinate';
import { MouseHandlerService } from '../services/mouse-handler.service';


@Component({
  selector: 'app-mouse',
  templateUrl: './mouse.component.html',
  styleUrls: ['./mouse.component.scss']
})
export class MouseComponent {

  color = 'green';
  width = 500;
  height = 500;

  constructor(private mouseService: MouseHandlerService) { }

  onMouseDown(event: MouseEvent) {

    const coordinateClick: Coordinate = { x: event.offsetX, y: event.offsetY };
    this.mouseService.onMouseDown(coordinateClick);

  }

  onMouseUp(event: MouseEvent) {

    const coordinateClick: Coordinate = { x: event.offsetX, y: event.offsetY };
    this.mouseService.onMouseUp(coordinateClick);

  }

}
