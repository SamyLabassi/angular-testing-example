import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BadMouseComponent } from './bad-mouse/bad-mouse.component';
import { MouseComponent } from './mouse/mouse.component';

@NgModule({
  declarations: [
    AppComponent,
    BadMouseComponent,
    MouseComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
